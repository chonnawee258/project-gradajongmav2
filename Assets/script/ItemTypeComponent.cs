using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chonnawee.GameDev3.chapter1
{
    public class ItemTypeComponent : MonoBehaviour
    {
        [SerializeField]
         protected ItemType m_ItemType;
             public ItemType Type
         {
             get
             {
                 return m_ItemType;
                 }
             set
             {
                 m_ItemType = value;
                
                 }
             }
    }
}