using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chonnawee.GameDev3.chapter6;
using Chonnawee.GameDev3.chapter5;
using Chonnawee.GameDev3.chapter1;

namespace Chonnawee.GameDev3.chapter6
{
    [RequireComponent(typeof(ItemTypeComponent))]
    public class Pickupable : MonoBehaviour , IInteractable, IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
             {
             //myself
             var itemTypeComponent = GetComponent <ItemTypeComponent >();
             //actor
             var inventory = actor.GetComponent <IInventory >();
             inventory.AddItem(itemTypeComponent.Type.ToString(),1);
            
             Destroy(gameObject);
             }
    
     public void ActorEnter(GameObject actor)
     {
        
         }
    
     public void ActorExit(GameObject actor)
     {
        
         }
    }
}